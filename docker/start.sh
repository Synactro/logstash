#!/bin/bash

source ./env.sh

sudo sysctl -w vm.max_map_count=262144

docker-compose down --remove-orphans  && docker-compose up -d

while ! nc -z localhost 5601; do
    echo "Waiting Kibana to launch on 5601..."
    sleep 1 # wait for 1/10 of the second before check again
done

while ! curl -X POST \
    http://localhost:5601/api/saved_objects/index-pattern/logstash-%2A \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -H 'Postman-Token: 529fd321-4e3a-4869-8b9d-d41121b2d947' \
    -H 'kbn-xsrf: anything' \
    -d '{"attributes":{"title":"logstash-*","timeFieldName":"@timestamp"}}'; do
    echo "Don't panic just wait..."
    sleep 10
done


while ! curl -X POST \
http://localhost:5601/api/saved_objects/search/Apache_Error \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 9f4bc0a4-a675-4749-b261-9fb2747f3918' \
-H 'kbn-xsrf: anything' \
-d '
{
    "attributes":
    {
        "title": "Apache Error",
        "description": "",
        "hits": 0,
        "columns": [
            "message",
            "level",
            "type"
        ],
        "sort": [
            "@timestamp",
            "desc"
        ],
        "version": 1,
        "kibanaSavedObjectMeta": {
            "searchSourceJSON": "{\"index\":\"logstash-*\",\"highlightAll\":true,\"version\":true,\"query\":{\"query\":\"\",\"language\":\"lucene\"},\"filter\":[{\"meta\":{\"negate\":false,\"index\":\"logstash-*\",\"type\":\"phrase\",\"key\":\"type\",\"value\":\"apache_error\",\"params\":{\"query\":\"apache_error\",\"type\":\"phrase\"},\"disabled\":false,\"alias\":null},\"query\":{\"match\":{\"type\":{\"query\":\"apache_error\",\"type\":\"phrase\"}}},\"$state\":{\"store\":\"appState\"}}]}"
        }
    }
}'; do
    echo "Don't panic just wait..."
    sleep 10
done

echo
echo 'logs : http://localhost:5601/app/kibana#/discover/Apache_Error';

echo 'Done!';